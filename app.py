from flask import Flask
from flask import request
from flask import json
from flask import Response

app = Flask(__name__)


@app.route('/', methods=["GET","POST"])
def get_json():
    ret = {'id': '0000', 'name': 'myself'}
    try:
        if request.method=="GET":
            if 'id' in request.args:
                ret['id'] = request.args['id']
            if 'name' in request.args:
                ret['name'] = request.args['name']

            return Response(json.dumps(ret), status=200, mimetype='application/json')
        elif request.method=="POST":
            if request.headers['Content-Type'] == 'application/json':
                return "echoing the json "+json.dumps(request.json)
            else:
                return "Inside post else"
    except Exception as e:
        ret = "error"
        return Response(json.dumps(ret),status=400,mimetype='application/json')

if __name__  == '__main__':
    app.run()